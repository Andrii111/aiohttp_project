from aiohttp import web
import json
import random


main_page = '''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Main page</title>
    <link rel="shortcut icon" href="data:image/x-icon;," type="image/x-icon"> 
</head>
<body>
<form action="/" method="post">
<h1>Short URL</h1>
<div>
<h3> the URL which has to be shortened</h3>
    <input type="text" name="long_url" placeholder="Enter the link here">
    <input type="submit" value="Go">
</div>
</form>
</body>
</html>
'''


async def main_url(request):
    return web.Response(text=main_page, content_type='text/html')


async def save_long_url(request):
    data = await request.post()
    long_url = data['long_url']
    short_url = ''.join(random.choice('0123456789abcdefghijklmnopqrstuvwxyz') for _ in range(6))
    try:
        with open('links.json') as json_file:
            data = json.loads(json_file.read())
    except FileNotFoundError:
        print('will make a new file')
    if len(data) >= 1:
        data['links'].append({short_url: long_url})
        with open('links.json', mode='w') as outfile:
            json.dump(data, outfile, ensure_ascii=False, indent=2)
    else:
        with open('links.json', mode='a') as json_file:
            json_file.write(json.dumps({'links': [{short_url: long_url}]}))
    return web.Response(text=short_url)


async def redirect_url(request):
    short_url = request.match_info['short_url']
    with open('links.json') as json_file:
        file_data = json.loads(json_file.read())
    for data in file_data['links']:
        long_url = data[short_url]
        raise web.HTTPFound(long_url)


app = web.Application()
app.add_routes([web.get('/', main_url)])
app.add_routes([web.post('/', save_long_url)])
app.add_routes([web.get('/{short_url}', redirect_url)])
web.run_app(app)
